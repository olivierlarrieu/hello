# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "hello"
VERSION = "1.0.0"

setup(
    name=NAME,
    version=VERSION,
    description="",
    author_email="",
    url="",
    keywords=[],
    install_requires=[],
    packages=find_packages(),
    package_data={'': []},
    include_package_data=True,
    entry_points={
        'console_scripts': []},
    long_description="""\
    This is a sample of setup.py and hello app
    """
)

