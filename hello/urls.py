from django.conf.urls import url
from hello.views import hello


urlpatterns = [
    url('hello', hello)
]
