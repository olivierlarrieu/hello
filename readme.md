# installation

## dev
    pip install --src=apps -e git+https://bitbucket.org/olivierlarrieu/hello.git#egg=hello

## prod
    pip install --target=apps git+https://bitbucket.org/olivierlarrieu/hello.git#egg=hello